cd ~
git clone http://github.com/username/dotvim.git ~/.vim
~/.vim/vimrc ~/.vimrc
ln -s ~/.vim/gvimrc ~/.gvimrc
cd ~/.vim
git submodule init
git submodule update
